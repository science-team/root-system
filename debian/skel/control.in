Source: root-system
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Lifeng Sun <lifongsun@gmail.com>,
Homepage: http://root.cern.ch
Build-Depends:
 autotools-dev,
 comerr-dev,
 debhelper (>= 5.0.37.2),
 graphviz,
 libavahi-core-dev,
 libfreetype6-dev,
 libgraphviz-dev,
 libltdl-dev,
 liblzma-dev,
 libncurses5-dev,
 libpcre3-dev,
 libsqlite3-dev,
 libssl-dev,
 libxext-dev,
 libxft-dev@builddepends@,
 libxpm-dev,
 lsb-release,
 po-debconf,
 python-dev (>= 2.1),
 ttf-freefont,
 x11proto-xext-dev,
 zlib1g-dev | libz-dev,
Standards-Version: 3.9.5
Vcs-Git: https://anonscm.debian.org/git/debian-science/packages/root-system.git
Vcs-Browser: https://anonscm.debian.org/cgit/debian-science/packages/root-system.git

Package: root-system
Architecture: all
Depends:
 libroot-core-dev,
 root-system-bin,
 ${misc:Depends},
Recommends:
 @plugins@,
Suggests:
 @extras@,
Description: metapackage to install all ROOT packages
 The ROOT system provides a set of OO frameworks with all the
 functionality needed to handle and analyze large amounts of data
 efficiently.
 .
 With the data defined as a set of objects, specialized storage methods
 can give direct access to the separate attributes of the selected
 objects, without having to touch the bulk of the data. Included are
 histogramming methods in 1, 2 and 3 dimensions, curve fitting, function
 evaluation, minimization, graphics and visualization classes to allow the
 easy creation of an analysis system that can query and process the data
 interactively or in batch mode.
 .
 The command language, the scripting (or macro) language, and the
 programming language are all C++, thanks to the built-in CINT C++
 interpreter. This interpreter removes the time consuming compile/link
 cycle, allowing for fast prototyping of the macros, and providing a
 good environment to learn C++. If more performance is needed, the
 interactively developed macros can be compiled using a C++ compiler.
 .
 The system has been designed in such a way that it can query its
 databases in parallel on MPP machines or on clusters of workstations
 or high-end PCs. ROOT is an open system that can be dynamically
 extended by linking external libraries. This makes ROOT a premier
 platform on which to build data acquisition, simulation and data
 analysis systems.
 .
 This package is a metapackage to ensure the installation of all
 possible ROOT packages on a system.

