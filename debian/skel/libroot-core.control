Package: libroot-core@libvers@
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 root-system-common,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 libroot-core-dev,
 root-system-bin,
Provides:
 libroot-core,
Description: Numerical data analysis framework - shared runtime libraries
 The ROOT system provides a set of OO frameworks with all the
 functionality needed to handle and analyze large amounts of data
 efficiently.
 .
 This package contains the shared libraries used by the ROOT
 system. The libraries contain numerous C++ class definitions for
 various purposes, some of which are:
    * System abstraction
    * Thin Thread API
    * Histograming
    * Persistent objects
    * GUI API
 and many others. Refer also to the package description of
 root-bin.

Package: libroot-core-dev
Section: libdevel
Architecture: any
Depends:
 cfortran,
 comerr-dev,
 g++ | c++-compiler,
 libfreetype6-dev,
 libkrb5-dev,
 libpcre3-dev,
 libssl-dev,
 libx11-dev,
 libxpm-dev,
 zlib1g-dev,
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Suggests:
 gccxml,
 root-system-bin,
Recommends:
 libroot-geom-dev,
 libroot-gui-dev,
 libroot-hist-dev,
 libroot-io-dev,
 libroot-net-dev,
 libroot-proof-dev,
 libroot-tree-dev,
Provides:
 libroot-dev,
Replaces:
 libroot-dev (<< 5.19.01-1),
Breaks:
 libroot-dev (<< 5.19.01-1),
Description: Header files for ROOT
 The ROOT system provides a set of OO frameworks with all the
 functionality needed to handle and analyze large amounts of data
 efficiently.
 .
 This package contains header files (Class declarations) for the ROOT
 class libraries. Please refer the package documentation for libroot
 and root-bin.

Package: libroot-static
Section: libdevel
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 libroot-core-dev,
Description: Static archive of ROOT libraries
 The ROOT system provides a set of OO frameworks with all the
 functionality needed to handle and analyze large amounts of data
 efficiently.
 .
 This package contains static archive of the ROOT classes.

